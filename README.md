# tpl450ioc

Define `PROD_NAME` in `CONFIG.local` file in the TOP folder of this project; see `EXAMPLE_CONFIG_local`.
It will be included by the `iocApp/src/Makefile` and used to name the IOC application binary file.

## test case

Stream:

http://172.30.244.89:8080/stream/video/mjpeg?resolution=HD&&Username=admin&&Password=QWRtaW4=&&tempid=0.004075305671139118

Static iamge:

http://172.30.244.89:8080/stream/snapshot.jpg?Username=admin&&Password=QWRtaW4=&&tempid=1
